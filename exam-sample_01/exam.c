/* vim: set tabstop=4 expandtab shiftwidth=4 softtabstop=4: */


/******************************************************************************/
/*** NOME:                                                                  ***/
/*** COGNOME:                                                               ***/
/*** MATRICOLA:                                                             ***/
/******************************************************************************/


#include <assert.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <upo/bst.h>
#include <upo/sort.h>
#include <stdbool.h>


/**** BEGIN of EXERCISE #1 ****/

static const void* upo_bst_predecessor_impl(const upo_bst_node_t *node, const void *key, upo_bst_comparator_t cmp)
{
    if (!node)
    {
        return NULL;
    }
    if (cmp(node->key, key) >= 0)
    {
        return upo_bst_predecessor_impl(node->left, key, cmp);
    }
    else
    {
        const void *right = upo_bst_predecessor_impl(node->right, key, cmp);
        return right ? right : node->key;
    }
}

const void* upo_bst_predecessor(const upo_bst_t bst, const void *key)
{
    return upo_bst_predecessor_impl(bst->root, key, upo_bst_get_comparator(bst));
}

/**** END of EXERCISE #1 ****/


/**** BEGIN of EXERCISE #2 ****/

void upo_swap(void *a, void *b, size_t n)
{
    unsigned char *aa = a;
    unsigned char *bb = b;
    for (size_t i = 0; i < n; ++i)
    {
        unsigned char tmp = aa[i];
        aa[i] = bb[i];
        bb[i] = tmp;
    }
}

void upo_bidi_bubble_sort(void *base, size_t n, size_t size, upo_sort_comparator_t cmp)
{
    unsigned char *_base = base;
    bool swapped;
    do
    {
        swapped = false;
        for (size_t i = 1; i < n; ++i)
        {
            if (cmp(_base + i * size, _base + (i - 1) * size) < 0)
            {
                upo_swap(_base + i * size, _base + (i - 1) * size, size);
                swapped = true;
            }
        }
        for (size_t i = n; i > 1; --i)
        {
            if (cmp(_base + (i - 1) * size, _base + (i - 2) * size) < 0)
            {
                upo_swap(_base + (i - 1) * size, _base + (i - 2) * size, size);
                swapped = true;
            }
        }
    } while (swapped);
}

/**** END of EXERCISE #2 ****/
