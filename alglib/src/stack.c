/* vim: set tabstop=4 expandtab shiftwidth=4 softtabstop=4: */

/*
 * Copyright 2015 University of Piemonte Orientale, Computer Science Institute
 *
 * This file is part of UPOalglib.
 *
 * UPOalglib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * UPOalglib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with UPOalglib.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "stack_private.h"
#include <stdio.h>
#include <stdlib.h>


upo_stack_t upo_stack_create()
{
    upo_stack_t stack = malloc(sizeof(struct upo_stack_s));
    if (stack == NULL)
    {
        perror("Unable to create a stack");
        abort();
    }

    stack->top = NULL;
    stack->size = 0;

    return stack;
}

void upo_stack_destroy(upo_stack_t stack, int destroy_data)
{
    if (stack != NULL)
    {
        upo_stack_clear(stack, destroy_data);
        free(stack);
    }
}

void upo_stack_push(upo_stack_t stack, void *data)
{
    if (stack) {
        upo_stack_node_t *second = stack->top;

        upo_stack_node_t *first = malloc(sizeof(upo_stack_node_t));
        first->data = data;
        first->next = second;

        stack->top = first;
        ++stack->size;
    }
}

void upo_stack_pop(upo_stack_t stack, int destroy_data)
{
    if (stack) {
        upo_stack_node_t *first = stack->top->next;
        if (destroy_data) {
            free(stack->top->data);
        }
        free(stack->top);
        stack->top = first;
        --stack->size;
    }
}

void* upo_stack_top(const upo_stack_t stack)
{
    return !upo_stack_is_empty(stack) ? stack->top->data : NULL;
}

int upo_stack_is_empty(const upo_stack_t stack)
{
    return stack ? stack->size == 0 : 1;
}

size_t upo_stack_size(const upo_stack_t stack)
{
    return stack ? stack->size : 0;
}

void upo_stack_clear(upo_stack_t stack, int destroy_data)
{
    while (!upo_stack_is_empty(stack)) {
        upo_stack_pop(stack, destroy_data);
    }
}
