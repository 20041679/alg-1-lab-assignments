/* vim: set tabstop=4 expandtab shiftwidth=4 softtabstop=4: */

/*
 * Copyright 2015 University of Piemonte Orientale, Computer Science Institute
 *
 * This file is part of UPOalglib.
 *
 * UPOalglib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * UPOalglib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with UPOalglib.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <assert.h>
#include "sort_private.h"
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <upo/utility.h>

void upo_insertion_sort(void *base, size_t n, size_t size, upo_sort_comparator_t cmp)
{
    unsigned char *last_copy = malloc(size);
    for (size_t i = 1; i < n; ++i) {
        unsigned char *curr = (unsigned char *)base + i * size;
        unsigned char *pos = upo_insertion_sort_insert_pos(base, curr, i + 1, size, cmp);
        memcpy(last_copy, curr, size);
        memmove(pos + size, pos, curr - pos);
        memcpy(pos, last_copy, size);
    }
    free(last_copy);
}

static void *upo_insertion_sort_insert_pos(
    const void *base,
    const void *elem,
    size_t n,
    size_t size,
    upo_sort_comparator_t cmp
)
{
    for (size_t i = 0; i < n; ++i) {
        if (cmp((unsigned char *)base + i * size, elem) >= 0) {
            return (unsigned char *)base + i * size;
        }
    }
    return NULL;
}

void upo_merge_sort(void *base, size_t n, size_t size, upo_sort_comparator_t cmp)
{
    if (n > 1) {
        void *base2 = (unsigned char *)base + (n / 2) * size;
        upo_merge_sort(base, n / 2, size, cmp);
        upo_merge_sort(base2, n - (n / 2), size, cmp);
        upo_merge_sort_merge(base, n / 2, base2, n - (n / 2), size, cmp);
    }
}

static void upo_merge_sort_merge(
    void *base1,
    size_t n1,
    void *base2,
    size_t n2,
    size_t size,
    upo_sort_comparator_t cmp
)
{
    unsigned char *tmp = malloc((n1 + n2) * size);

    size_t i1 = 0;
    size_t i2 = 0;

    while (i1 < n1 && i2 < n2) {
        unsigned char *curr1 = (unsigned char *)base1 + i1 * size;
        unsigned char *curr2 = (unsigned char *)base2 + i2 * size;
        unsigned char *currtmp = tmp + (i1 + i2) * size;
        if (cmp(curr1, curr2) <= 0) {
            memcpy(currtmp, curr1, size);
            ++i1;
        } else {
            memcpy(currtmp, curr2, size);
            ++i2;
        }
    }

    while (i1 < n1) {
        unsigned char *curr = (unsigned char *)base1 + i1 * size;
        unsigned char *currtmp = tmp + (i1 + i2) * size;
        memcpy(currtmp, curr, size);
        ++i1;
    }

    while (i2 < n2) {
        unsigned char *curr = (unsigned char *)base2 + i2 * size;
        unsigned char *currtmp = tmp + (i1 + i2) * size;
        memcpy(currtmp, curr, size);
        ++i2;
    }

    memcpy(base1, tmp, n1 * size);
    memcpy(base2, tmp + n1 * size, n2 * size);
    free(tmp);
}

void upo_quick_sort(void *base, size_t n, size_t size, upo_sort_comparator_t cmp)
{
    unsigned char *_base = base;
    upo_quick_sort_rec(_base, _base + (n - 1) * size, size, cmp);
}

static void upo_quick_sort_rec(
    void *lo,
    void *hi,
    size_t size,
    upo_sort_comparator_t cmp
)
{
    if (lo <= hi) {
        unsigned char *p = upo_quick_sort_partition(lo, hi, size, cmp);
        if (p > (unsigned char *)lo) {
            upo_quick_sort_rec(lo, p - size, size, cmp);
        }
        upo_quick_sort_rec(p + size, hi, size, cmp);
    }
}

// using hoare's scheme
static void *upo_quick_sort_partition(
    void *lo,
    void *hi,
    size_t size,
    upo_sort_comparator_t cmp
)
{
    unsigned char *p = lo;
    unsigned char *l = lo;
    unsigned char *r = (unsigned char *)hi + size;
    while (1) {
        do { l += size; } while (l < (unsigned char *)hi && cmp(l, p) < 0);
        do { r -= size; } while (r > (unsigned char *)lo && cmp(r, p) > 0);
        if (l >= r) { break; }
        upo_swap(l, r, size);
    }
    upo_swap(p, r, size);
    return r;
}
