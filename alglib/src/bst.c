/* vim: set tabstop=4 expandtab shiftwidth=4 softtabstop=4: */

/*
 * Copyright 2015 University of Piemonte Orientale, Computer Science Institute
 *
 * This file is part of UPOalglib.
 *
 * UPOalglib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * UPOalglib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with UPOalglib.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "bst_private.h"
#include <stdio.h>
#include <stdlib.h>


/**** EXERCISE #1 - BEGIN of FUNDAMENTAL OPERATIONS ****/

static upo_bst_node_t *upo_bst_node_create(void *key, void *value)
{
    upo_bst_node_t *ret = malloc(sizeof(*ret));
    if (!ret) {
        perror("Unable to create new node");
        abort();
    }
    ret->key = key;
    ret->value = value;
    ret->left = NULL;
    ret->right = NULL;
    return ret;
}

static void upo_bst_node_destroy(upo_bst_node_t *node, int destroy_data)
{
    if (destroy_data) {
        free(node->key);
        free(node->value);
    }
    free(node);
}

upo_bst_t upo_bst_create(upo_bst_comparator_t key_cmp)
{
    upo_bst_t tree = malloc(sizeof(struct upo_bst_s));
    if (tree == NULL)
    {
        perror("Unable to create a binary search tree");
        abort();
    }

    tree->root = NULL;
    tree->key_cmp = key_cmp;

    return tree;
}

void upo_bst_destroy(upo_bst_t tree, int destroy_data)
{
    if (tree != NULL)
    {
        upo_bst_clear(tree, destroy_data);
        free(tree);
    }
}

static void upo_bst_clear_impl(upo_bst_node_t *node, int destroy_data)
{
    if (node != NULL)
    {
        upo_bst_clear_impl(node->left, destroy_data);
        upo_bst_clear_impl(node->right, destroy_data);

        upo_bst_node_destroy(node, destroy_data);
    }
}

void upo_bst_clear(upo_bst_t tree, int destroy_data)
{
    if (tree != NULL)
    {
        upo_bst_clear_impl(tree->root, destroy_data);
        tree->root = NULL;
    }
}

static upo_bst_node_t *upo_bst_put_impl(
    upo_bst_node_t *node,
    void *key,
    void *value,
    void **value_old,
    upo_bst_comparator_t cmp
)
{
    *value_old = NULL;
    if (!node) {
        return upo_bst_node_create(key, value);
    }
    int res = cmp(key, node->key);
    if (res < 0) {
        node->left = upo_bst_put_impl(node->left, key, value, value_old, cmp);
    } else if (res > 0) {
        node->right = upo_bst_put_impl(node->right, key, value, value_old, cmp);
    } else {
        *value_old = node->value;
        node->value = value;
    }
    return node;
}

void *upo_bst_put(upo_bst_t tree, void *key, void *value)
{
    void *value_old;
    tree->root = upo_bst_put_impl(
        tree->root,
        key,
        value,
        &value_old,
        upo_bst_get_comparator(tree)
    );
    return value_old;
}

static upo_bst_node_t *upo_bst_insert_impl(
    upo_bst_node_t *node,
    void *key,
    void *value,
    upo_bst_comparator_t cmp
)
{
    if (!node)
    {
        return upo_bst_node_create(key, value);
    }
    int res = cmp(key, node->key);
    if (res < 0) {
        node->left = upo_bst_insert_impl(node->left, key, value, cmp);
    } else if (res > 0) {
        node->right = upo_bst_insert_impl(node->right, key, value, cmp);
    }
    return node;
}

void upo_bst_insert(upo_bst_t tree, void *key, void *value)
{
    tree->root = upo_bst_insert_impl(
        tree->root,
        key,
        value,
        upo_bst_get_comparator(tree)
    );
}

static void *upo_bst_get_impl(
    upo_bst_node_t *node,
    const void *key,
    upo_bst_comparator_t cmp
)
{
    if (!node) {
        return NULL;
    }
    int res = cmp(key, node->key);
    if (res < 0) {
        return upo_bst_get_impl(node->left, key, cmp);
    }
    if (res > 0) {
        return upo_bst_get_impl(node->right, key, cmp);
    }
    return node->value;
}

void* upo_bst_get(const upo_bst_t tree, const void *key)
{
    return upo_bst_get_impl(tree->root, key, upo_bst_get_comparator(tree));
}

int upo_bst_contains(const upo_bst_t tree, const void *key)
{
    return upo_bst_get(tree, key) ? 1 : 0;
}

static upo_bst_node_t *upo_bst_delete_2_c_impl(
    upo_bst_node_t *node,
    upo_bst_comparator_t cmp,
    int destroy_data
)
{
    upo_bst_node_t *max = upo_bst_max_impl(node->left);
    node->key = max->key;
    node->value = max->value;
    node->left = upo_bst_delete_impl(node->left, max->key, cmp, destroy_data);
    return node;
}

static upo_bst_node_t *upo_bst_delete_1_c_impl(
    upo_bst_node_t *node,
    int destroy_data
)
{
    upo_bst_node_t *hold = node;
    if (node->left) { node = node->left; }
    else { node = node->right; }
    upo_bst_node_destroy(hold, destroy_data);
    return node;
}

static upo_bst_node_t *upo_bst_delete_impl(
    upo_bst_node_t *node,
    const void *key,
    upo_bst_comparator_t cmp,
    int destroy_data
)
{
    if (!node) {
        return NULL;
    }
    int res = cmp(key, node->key);
    if (res < 0) {
        node->left = upo_bst_delete_impl(node->left, key, cmp, destroy_data);
    } else if (res > 0) {
        node->right = upo_bst_delete_impl(node->right, key, cmp, destroy_data);
    } else if (node->left && node->right) {
        node = upo_bst_delete_2_c_impl(node, cmp, destroy_data);
    } else {
        node = upo_bst_delete_1_c_impl(node, destroy_data);
    }
    return node;
}

void upo_bst_delete(upo_bst_t tree, const void *key, int destroy_data)
{
    tree->root = upo_bst_delete_impl(
        tree->root,
        key,
        upo_bst_get_comparator(tree),
        destroy_data
    );
}

static size_t upo_bst_size_impl(upo_bst_node_t *node)
{
    return node
        ? 1 + upo_bst_size_impl(node->left) + upo_bst_size_impl(node->right)
        : 0;
}

size_t upo_bst_size(const upo_bst_t tree)
{
    return tree ? upo_bst_size_impl(tree->root) : 0;
}

static size_t upo_bst_height_impl(upo_bst_node_t *node) 
{
    if (!node || !(node->left || node->right)) {
        return 0;
    }
    size_t height_l = upo_bst_size_impl(node->left);
    size_t height_r = upo_bst_size_impl(node->right);
    return height_l > height_r ? height_l : height_r;
}

size_t upo_bst_height(const upo_bst_t tree)
{
    return upo_bst_height_impl(tree->root);
}

void upo_bst_traverse_in_order_impl(
    upo_bst_node_t *node,
    upo_bst_visitor_t visit,
    void *visit_context
)
{
    if (node) {
        upo_bst_traverse_in_order_impl(node->left, visit, visit_context);
        visit(node->key, node->value, visit_context);
        upo_bst_traverse_in_order_impl(node->right, visit, visit_context);
    }
}

void upo_bst_traverse_in_order(
    const upo_bst_t tree,
    upo_bst_visitor_t visit,
    void *visit_context
)
{
    upo_bst_traverse_in_order_impl(tree->root, visit, visit_context);
}

int upo_bst_is_empty(const upo_bst_t tree)
{
    return tree ? !tree->root : 1;
}

/**** EXERCISE #1 - END of FUNDAMENTAL OPERATIONS ****/


/**** EXERCISE #2 - BEGIN of EXTRA OPERATIONS ****/

static upo_bst_node_t *upo_bst_min_impl(upo_bst_node_t *node)
{
    if (!node) {
        return NULL;
    }
    if (node->left) {
        return upo_bst_min_impl(node->left);
    }
    return node;
}

void* upo_bst_min(const upo_bst_t tree)
{
    upo_bst_node_t *node = upo_bst_min_impl(tree->root);
    return node ? node->key : NULL;
}

static upo_bst_node_t *upo_bst_max_impl(upo_bst_node_t *node)
{
    if (!node) {
        return NULL;
    }
    if (node->right) {
        return upo_bst_max_impl(node->right);
    }
    return node;
}

void* upo_bst_max(const upo_bst_t tree)
{
    upo_bst_node_t *node = upo_bst_max_impl(tree->root);
    return node ? node->key : NULL;
}

static upo_bst_node_t *upo_bst_delete_min_impl(
    upo_bst_node_t *node,
    int destroy_data
)
{
    if (!node) {
        return NULL;
    }
    if (!node->left) {
        upo_bst_node_t *ret = node->right;
        upo_bst_node_destroy(node, destroy_data);
        return ret;
    }
    node->left = upo_bst_delete_min_impl(node->left, destroy_data);
    return node;
}

void upo_bst_delete_min(upo_bst_t tree, int destroy_data)
{
    tree->root = upo_bst_delete_min_impl(tree->root, destroy_data);
}

static upo_bst_node_t *upo_bst_delete_max_impl(
    upo_bst_node_t *node,
    int destroy_data
)
{
    if (!node) {
        return NULL;
    }
    if (!node->right) {
        upo_bst_node_t *ret = node->left;
        upo_bst_node_destroy(node, destroy_data);
        return ret;
    }
    node->right = upo_bst_delete_max_impl(node->right, destroy_data);
    return node;
}

void upo_bst_delete_max(upo_bst_t tree, int destroy_data)
{
    tree->root = upo_bst_delete_max_impl(tree->root, destroy_data);
}

static void *upo_bst_floor_impl(
    upo_bst_node_t *node,
    const void *key,
    upo_bst_comparator_t cmp
)
{
    if (!node) {
        return NULL;
    }
    int res = cmp(key, node->key);
    if (res < 0) {
        return upo_bst_floor_impl(node->left, key, cmp);
    }
    if (res > 0) {
        void *edge = upo_bst_floor_impl(node->right, key, cmp);
        return edge ? edge : node->key;
    }
    return node->key;
}

void* upo_bst_floor(const upo_bst_t tree, const void *key)
{
    return upo_bst_floor_impl(tree->root, key, upo_bst_get_comparator(tree));
}

static void *upo_bst_ceil_impl(
    upo_bst_node_t *node,
    const void *key,
    upo_bst_comparator_t cmp
)
{
    if (!node) {
        return NULL;
    }
    int res = cmp(key, node->key);
    if (res < 0) {
        void *edge = upo_bst_ceil_impl(node->left, key, cmp);
        return edge ? edge : node->key;
    }
    if (res > 0) {
        return upo_bst_ceil_impl(node->right, key, cmp);
    }
    return node->key;
}

void* upo_bst_ceiling(const upo_bst_t tree, const void *key)
{
    return upo_bst_ceil_impl(tree->root, key, upo_bst_get_comparator(tree));
}

static upo_bst_key_list_t upo_bst_key_list_add(
    upo_bst_key_list_t list,
    void *key
)
{
    upo_bst_key_list_t ret;
    ret = malloc(sizeof(upo_bst_key_list_node_t));
    ret->key = key;
    ret->next = list;
    return ret;
}

upo_bst_key_list_t upo_bst_keys_range_impl(
    upo_bst_node_t *node,
    const void *low_key,
    const void *high_key,
    upo_bst_key_list_t list,
    upo_bst_comparator_t cmp
)
{
    if (!node) {
        return list;
    }
    if (cmp(node->key, low_key) < 0) {
        // outside lower bound, exclude left subtree
        return upo_bst_keys_range_impl(
            node->right,
            low_key,
            high_key,
            list,
            cmp
        );
    }
    if (cmp(node->key, high_key) > 0) {
        // outside upper bound, exclude right subtree
        return upo_bst_keys_range_impl(
            node->left,
            low_key,
            high_key,
            list,
            cmp
        );
    }
    // inside range
    return upo_bst_keys_range_impl(
        node->right,
        low_key,
        high_key,
        upo_bst_keys_range_impl(
            node->left,
            low_key,
            high_key,
            upo_bst_key_list_add(list, node->key),
            cmp
        ),
        cmp
    );
}

upo_bst_key_list_t upo_bst_keys_range(
    const upo_bst_t tree,
    const void *low_key,
    const void *high_key
)
{
    return upo_bst_keys_range_impl(
        tree->root,
        low_key,
        high_key,
        NULL,
        upo_bst_get_comparator(tree)
    );
}

static void upo_bst_key_list_add_key_visit(
    void *key,
    void *value,
    void *context
)
{
    (void)value;
    upo_bst_key_list_t *list = context;
    *list = upo_bst_key_list_add(*list, key);
}

upo_bst_key_list_t upo_bst_keys(const upo_bst_t tree)
{
    upo_bst_key_list_t ret = NULL;
    upo_bst_traverse_in_order(tree, upo_bst_key_list_add_key_visit, &ret);
    return ret;
}

static int upo_bst_is_bst_impl(
    upo_bst_node_t *node,
    const void *min_key,
    const void *max_key,
    upo_bst_comparator_t cmp
)
{
    if (!node) {
        return 1;
    }
    if (cmp(node->key, min_key) < 0 || cmp(node->key, max_key) > 0) {
        return 0;
    }
    return upo_bst_is_bst_impl(node->left, min_key, node->key, cmp)
        && upo_bst_is_bst_impl(node->right, node->key, max_key, cmp);
}

int upo_bst_is_bst(
    const upo_bst_t tree,
    const void *min_key,
    const void *max_key
)
{
    return upo_bst_is_bst_impl (
        tree->root,
        min_key,
        max_key,
        upo_bst_get_comparator(tree)
    );
}

/**** EXERCISE #2 - END of EXTRA OPERATIONS ****/


upo_bst_comparator_t upo_bst_get_comparator(const upo_bst_t tree)
{
    if (tree == NULL)
    {
        return NULL;
    }

    return tree->key_cmp;
}
