/* vim: set tabstop=4 expandtab shiftwidth=4 softtabstop=4: */

/**
 * \file apps/postfix_eval.c
 *
 * \brief An application to evaluate postfix mathematical expressions.
 *
 * \author Your Name
 *
 * \copyright 2015 University of Piemonte Orientale, Computer Science Institute
 *
 * This file is part of UPOalglib.
 *
 * UPOalglib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * UPOalglib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with UPOalglib.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <assert.h>
#include <ctype.h>
#include <math.h>
#include "postfix_eval.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <upo/stack.h>


#define MAX_LINE_LEN 256

#define ok_or_return(ok) do { if(!ok) { return ok; } } while (0)

long int wrap_sum(long int a, long int b) {
    return a + b;
}

long int wrap_diff(long int a, long int b) {
    return a - b;
}

long int wrap_mul(long int a, long int b) {
    return a * b;
}

long int wrap_div(long int a, long int b) {
    return a / b;
}

long int wrap_mod(long int a, long int b) {
    return a % b;
}

long int wrap_pow(long int a, long int b) {
    return (long int)powl((long double)a, (long double)b);
}

long int (*map(char op)) (long int, long int) {
    switch (op) {
        case '+':
            return wrap_sum;
        case '-':
            return wrap_diff;
        case '*':
            return wrap_mul;
        case '/':
            return wrap_div;
        case '%':
            return wrap_mod;
        case '^':
            return wrap_pow;
        default:
            return NULL;
    }
}

void push(upo_stack_t operands, long int op) {
    void *data = malloc(sizeof(op));
    *((long int *)data) = op;
    upo_stack_push(operands, data);
}

int pop(upo_stack_t operands, long int *op) {
    if (upo_stack_is_empty(operands)) { return 0; }
    *op = *((long int *)upo_stack_top(operands));
    upo_stack_pop(operands, 1);
    return 1;
}

int binary(upo_stack_t operands, long int *op1, long int *op2) {
    ok_or_return(pop(operands, op2));
    return pop(operands, op1);
}

int eval_postfix(const char *expr, long *res)
{
    size_t i = 0;
    upo_stack_t operands = upo_stack_create();

    while (expr[i]) {
        long int (*f)(long int, long int);
        if ((f = map(expr[i]))) {
            long int op1, op2;
            ok_or_return(binary(operands, &op1, &op2));
            push(operands, f(op1, op2));
            ++i;
        } else if isdigit(expr[i]) {
            char *end;
            long int op = strtol(&expr[i], &end, 10);
            push(operands, op);
            i = end - expr;
        } else {
            ++i;
        }
    }

    int ret = 1;;
    if (!(ret = pop(operands, res))) {
        ret = 0;
        goto clean;
    }
    if (!(ret = upo_stack_is_empty(operands))) {
        ret = 0;
        goto clean;
    }
clean:
    upo_stack_destroy(operands, 1);
    return ret;
}


void eval_lines(FILE *fp)
{
    char line[MAX_LINE_LEN];

    while (fgets(line, sizeof(line), fp))
    {
        size_t slen = strlen(line);
        int ok = 0;
        long res = 0;
        char *res_str = NULL;
        long expect_res = 0;

        /* Strips the terminating newline (if any) */
        if (slen > 0 && line[slen-1] == '\n')
        {
            line[slen-1] = '\0';
            --slen;
        }

        /* Checks if we have the expected result. In this case the input line should be: <expr>, <result> */
        res_str = strstr(line, "=>");
        if (res_str != NULL)
        {
            line[res_str-line] = '\0';
            res_str += 2;
            expect_res = atol(res_str);
        }

        ok = eval_postfix(line, &res);
        if (ok)
        {
            if (res_str != NULL)
            {
                printf("Expression '%s' -> %ld (expected: %ld -> %s)\n", line, res, expect_res, (res == expect_res) ? "OK" : "KO");
            }
            else
            {
                printf("Expression '%s' -> %ld\n", line, res);
            }
        }
        else
        {
            printf("Expression '%s' -> Malformed\n", line);
        }
    }
}

void usage(const char *progname)
{
    fprintf(stderr, "Usage: %s -f [<filename>]\n", progname);
    fprintf(stderr, "Options:\n");
    fprintf(stderr, "-f <filename>: The full path name to the file containing strings (one for each line).\n"
                    "               If not given, strings are read from standard input.\n"
                    "               [default: standard input]\n");
    fprintf(stderr, "-h: Displays this message\n");
    fprintf(stderr, "-v: Enables output verbosity\n");
}


int main(int argc, char *argv[])
{
    char *opt_filename = NULL;
    int opt_help = 0;
    int opt_verbose = 0;
    int arg = 0;
    FILE *fp = NULL;

    for (arg = 1; arg < argc; ++arg)
    {
        if (!strcmp("-f", argv[arg]))
        {
            ++arg;
            if (arg >= argc)
            {
                fprintf(stderr, "ERROR: expected file name.\n");
                usage(argv[0]);
                return EXIT_FAILURE;
            }
            opt_filename = argv[arg];
        }
        else if (!strcmp("-h", argv[arg]))
        {
            opt_help = 1;
        }
        else if (!strcmp("-v", argv[arg]))
        {
            opt_verbose = 1;
        }
    }

    if (opt_help)
    {
        usage(argv[0]);
        return EXIT_SUCCESS;
    }

    if (opt_verbose)
    {
        printf("-- Options:\n");
        printf("* File name: %s\n", opt_filename ? opt_filename : "<not given>");
    }

 
    if (opt_filename != NULL)
    {
        fp = fopen(opt_filename, "r");
        if (fp == NULL)
        {
            perror("ERROR: cannot open input file");
            return EXIT_FAILURE;
        }
    }
    else
    {
        fp = stdin;
    }

    if (opt_verbose)
    {
        printf("-- Evaluating...\n");
    }

    eval_lines(fp);

    if (opt_filename != NULL)
    {
        fclose(fp);
    }

    return EXIT_SUCCESS;
}
